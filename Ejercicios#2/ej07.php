<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 7</title>
</head>
<body>
    <?php
        $parcial1 = rand(0,30);
        $parcial2 = rand(0,20);
        $final1 = rand(0,50);
        echo "Puntaje del parcial 1: $parcial1" . "<br>";
        echo "Puntaje del parcial 2: $parcial2" . "<br>";
        echo "Puntaje del final: $final1" . "<br>";
        switch ($sum=$parcial1 + $parcial2 + $final1) {
            case $sum<59:
                echo "Nota: 1";
                break;
            case $sum<69:
                echo "Nota: 2";
                break;
            case $sum<79:
                echo "Nota: 3";
                break;
            case $sum<89:
                echo "Nota: 4";
                break;
            case $sum<=100:
                echo "Nota: 5";
                break;
        }
    ?>
</body>
</html>