<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        echo "<table border='3'>";
        echo "<h2>TABLA DEL 9</h2>";
        for ($i=1; $i < 11; $i++) { 
            $multiplicacion = 9*$i;
            if ($i%2==0) {
                echo "<tr style='background-color: grey'>";
                echo "<td> 9 x $i </td>";
                echo "<td> $multiplicacion </td>";
                echo "</tr>";
            }else{
                echo "<tr>";
                echo "<td> 9 x $i </td>";
                echo "<td> $multiplicacion </td>";
                echo "</tr>";
            }
        }
        echo "</table>";
    ?>
</body>
</html>