<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        $num = 24.5;
        if (is_float($num)){
            echo "La variable con valor: $num, es decimal";
        }
        echo "<br>";
        $num = "HOLA";
        if(is_string($num)){
            echo "La variable que ahora tiene el valor: $num, es string";
        }
        settype($num,"integer");
        echo "<br>";
        echo var_dump($num);
    ?>
</body>
</html>