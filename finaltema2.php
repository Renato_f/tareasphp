<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Final - Tema 2</title>
</head>
<body>
<?php
    class Vehiculo {
        protected $marca;
        protected $modelo;
        protected $color;
        protected $velocidad;

        public function __construct($marca, $modelo, $color, $velocidad) {
            $this->marca = $marca;
            $this->modelo = $modelo;
            $this->color = $color;
            $this->velocidad = $velocidad;
        }

        public function getMarca() {
            return $this->marca;
        }

        public function getModelo() {
            return $this->modelo;
        }

        public function getColor() {
            return $this->color;
        }

        public function getVelocidad() {
            return $this->velocidad;
        }
    }

    class Automovil extends Vehiculo {
        private $numeroPuertas;

        public function __construct($marca, $modelo, $color, $velocidad, $numeroPuertas) {
            parent::__construct($marca, $modelo, $color, $velocidad);
            $this->numeroPuertas = $numeroPuertas;
        }

        public function getNumeroPuertas() {
            return $this->numeroPuertas;
        }
    }

    $auto = new Automovil("Ford", "Focus", "Rojo", 120, 4);

    echo "Marca: " . $auto->getMarca() . "<br>";
    echo "Modelo: " . $auto->getModelo() . "<br>";
    echo "Color: " . $auto->getColor() . "<br>";
    echo "Velocidad: " . $auto->getVelocidad() . " km/h<br>";
    echo "Numero de puertas: " . $auto->getNumeroPuertas() . "<br>";
?>
</body>
</html>
