<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Final</title>
</head>
<body>
    
<?php
    $host = "localhost";
    $dbname = "finalSegunda";
    $username = "postgres";
    $password = "admin";
    $dsn = "pgsql:host=$host;dbname=$dbname";
    $conn = new PDO($dsn, $username, $password);

    //Insercion de 50 cursos
    $cursos = array("Matematica", "Quimica", "Etica", "Fisica", "Biologia", "Historia", "Geografia", "Ingles", "Espanol", "Lenguaje", "Artes", "Ciencias politicas", "Misterio Cristiano", "Algebra lineal");
    $cursos_insertados = array();

    $contador = 0;
    while ($contador<50) {
        $nombre = $cursos[array_rand($cursos)];
        $numero = rand(1, 4);
        $nombre = $nombre." ".$numero;
        $ano = rand(1, 4);
        $seccion = chr(rand(65, 90));
        
        if(!in_array($nombre, $cursos_insertados)){
            $sql = "INSERT INTO Cursos (nombre, ano, seccion) VALUES ('$nombre', $ano, '$seccion')";
            $conn->exec($sql);
            array_push($cursos_insertados, $nombre);
            $contador++;
        }
    }
    echo "Insercion de 50 cursos aleatorios completada";

    //Insercion de alumnos


    // Crear arrays con nombres y apellidos aleatorios
    $nombres = array("Juan", "Maria", "Pedro", "Ana", "Luis", "Sofia", "Carlos", "Isabel", "Andres", "Gabriela", "Diego", "Paula", "Alejandro", "Camila", "Santiago", "Valeria", "Adrian", "Ximena", "Sebastian", "Catalina", "Miguel", "Violeta", "Daniel", "Bianca");
    $apellidos = array("Garcia", "Rodriguez", "Martinez", "Gonzalez", "Lopez", "Perez", "Sanchez", "Ramirez", "Torres", "Gomez", "Diaz", "Vargas", "Castro", "Ruiz", "Alvarez", "Jimenez", "Moreno", "Medina", "Aguilar", "Mendoza", "Fernandez", "Ortiz", "Estrada", "Herrera", "Avila");

    // Insertar 200 registros aleatorios
    $contadorAlumnos = 0;
    $matricula_insertada = array();
    $alumnos_insertados = array();
    while($contadorAlumnos < 200) {
        $nombre = $nombres[array_rand($nombres)];
        $apellido = $apellidos[array_rand($apellidos)];
        $nombreCompleto = $nombre." ".$apellido;
        $matricula = "C" . rand(10000, 99999);

        if(!in_array($matricula, $matricula_insertada) and !in_array($nombreCompleto, $alumnos_insertados)){
            $sql = "INSERT INTO alumnos (nombre, apellido, matricula) VALUES ('$nombre', '$apellido', '$matricula')";
            $conn->exec($sql);
            array_push($matricula_insertada, $matricula);
            array_push($alumnos_insertados, $nombreCompleto);
            $contadorAlumnos++;
        }
    }
    echo "Insercion de 200 alumnos aleatorios completada";

    //Insertar 500 inscripciones

    // Genera una fecha aleatoria dentro del último año
function randomDate() {
    return date("Y-m-d", rand(strtotime("-1 year"), time()));
}

// Genera un id de estudiante y un id de curso aleatorios
function randomStudent() {
    return rand(1, 200);
}
function randomCourse() {
    return rand(1, 50);
}

$combinacion = array();

// insercion de 500 regs
for ($i = 0; $i < 500; $i++) {
    $student_id = randomStudent();
    $course_id = randomCourse();
    $date = randomDate();
    $active = rand(0, 1) == 1;
    if(!isset($combinacion[$student_id][$course_id])){
        $query = "INSERT INTO inscriptions (curso_id, alumno_id, fecha, activo) VALUES ($course_id, $student_id, '$date', $active)";
        $result = pg_query($query);
        if (!$result) {
            echo "Error: " . pg_last_error();
        }
        $combinacion[$student_id][$course_id] = true;
    }
    else{
        $i--;
    }
}

echo "500 records inserted successfully.";
?>
        
</body>
</html>
