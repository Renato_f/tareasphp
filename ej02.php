<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
            echo '
    <style>
        table {
            border-collapse: collapse;
        }
        tbody {
            border: black 1px solid;
        }
        .demo th {
            border: black 1px solid;
            background-color: #C8C8C8;
        }
        .demo td {
            border: black 1px solid;
        }
        th[scope="rowgroup"]{
            background-color: yellow;
        }
    </style>
    <table class="demo">
        <thead>
            <tr>
            <th colspan="3" scope="rowgroup">Productos</th>
            </tr>
            <tr>
                <th>Nombre</th>     
                <th>Cantidad</th>   
                <th>Precio (Gs)</th> 
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Coca Cola</td>
                <td style="text-align:center">100</td>
                <td style="text-align:center">4500</td>
            </tr>
            <tr style="background-color: #90EE90">
                <td>Pepsi</td>
                <td style="text-align:center">30</td>
                <td style="text-align:center">4800</td>
            </tr>
            <tr>
                <td>Sprite</td>
                <td style="text-align:center">20</td>
                <td style="text-align:center">4500</td>
            </tr>
            <tr style="background-color: #90EE90">
                <td>Guaraná</td>
                <td style="text-align:center">200</td>
                <td style="text-align:center">4500</td>
            </tr>
            <tr>
                <td>SevenUp</td>
                <td style="text-align:center">24</td>
                <td style="text-align:center">4800</td>
            </tr>
            <tr style="background-color: #90EE90">
                <td>Mirinda Naranja</td>
                <td style="text-align:center">56</td>
                <td style="text-align:center">4800</td>
            </tr>
            <tr>
                <td>Mirinda Guaraná</td>
                <td style="text-align:center">89</td>
                <td style="text-align:center">4800</td>
            </tr>
            <tr style="background-color: #90EE90">
                <td>Fanta Naranja</td>
                <td style="text-align:center">10</td>
                <td style="text-align:center">4500</td>
            </tr>
            <tr>
                <td>Fanta Piña</td>
                <td style="text-align:center">2</td>
                <td style="text-align:center">4500</td>
            </tr>
        <tbody>
    </table>';
    ?>
</body>
</html>