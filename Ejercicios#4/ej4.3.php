<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ej4.3</title>
</head>
<body>
    <?php
        
        $conn = pg_connect("host=localhost port=5432 dbname=ejercicio1 user=postgres password=admin");

        if (!$conn) {
            echo "Ocurrio un error en la conexion!";
            exit;
        }
        $res = pg_query($conn," SELECT pr.nombre, pr.precio, mr.nombre, em.nombre, cat.nombre 
                                FROM producto pr, marca mr, empresa em, categoria cat 
                                where pr.id_marca = mr.id_marca and mr.id_empresa = em.id_empresa and pr.id_categoria = cat.id_categoria");

        echo "<h2>Tabla de productos</h2>";
        echo "<table border='3'>";
        echo "<tr style='background-color: grey'>";
            echo "<td>Nombre producto</td>";
            echo "<td>Precio</td>";
            echo "<td>Marca</td>";
            echo "<td>Empresa</td>";
            echo "<td>Categoria</td>";
        while($row = pg_fetch_row($res)){
            echo "<tr style='background-color: white'>";
            echo "<td>$row[0]</td>";
            echo "<td>$row[1]</td>";
            echo "<td>$row[2]</td>";
            echo "<td>$row[3]</td>";
            echo "<td>$row[4]</td>";
        }
            
        
        echo "</table>";
        pg_close($conn);
        
    ?>
</body>
</html>