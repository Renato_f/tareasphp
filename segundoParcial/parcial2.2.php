<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EJ 2 PARCIAL 2 - Renato FERRER</title>
</head>
<body>
    <?php
        /*Implementar un script PHP que haga lo siguiente:
        • Inserte en la tabla alumnos creada anteriormente: 12 alumnos con valores válidos
        cualesquiera (1 punto)
        • Visualice de manera tabular los 12 alumnos creados en al paso previo (1 punto).
        • Crear una función que retorne el nombre y el apellido del alumno con mayor edad y el
        nombre y apellido del alumno con menor edad (si hay edades iguales se elige uno al
        azar) (1 punto). */
        function make_seed(){
            list($usec, $sec) = explode(' ', microtime());
            return (float) $sec + ((float) $usec * 100000);
        }
        mt_srand(make_seed());

        $conn = pg_connect("host=localhost port=5432 dbname=examen user=postgres password=admin");

        if (!$conn) {
            echo "Ocurrio un error en la conexion!";
            exit;
        }
        echo "<table border='3'>";
        echo "<tr style='background-color: grey'>";
                echo "<td>Id</td>";
                echo "<td>Nombre</td>";
                echo "<td>Apellido</td>";
                echo "<td>Edad</td>";
        for ($i=1; $i <= 12; $i++) { 
            try {
                $edadRandom = mt_rand(7,20); //supongo que un alumno puede tener entre 7 y 20 años jaja
                $insertQuery = "INSERT INTO alumnos VALUES ($i,'alumno$i','apellido$i', $edadRandom)";
                $res = pg_query($insertQuery);
                echo "<tr style='background-color: white'>";
                echo "<td>$i</td>";
                echo "<td>Alumno$i</td>";
                echo "<td>Apellido$i</td>";
                echo "<td>$edadRandom</td>";
            } catch (PDOException $e) {
                echo "ERROR:: ". $e->getMessage();
            }
            
        }
        echo "</table>";
        function returnMayorYMenor(){
            $retrieveQueryMayor = "SELECT * FROM alumnos ORDER BY edad DESC limit 1;"; //MAYOR ALUMNO, me asegure que solo traiga 1 en caso de que hayan 2 iguales
            $resMayor = pg_query($retrieveQueryMayor);
            $row1 = pg_fetch_row($resMayor);
            echo "MAYOR alumno con nombre: ", $row1[1], ", de apellido: ",$row1[2], ", tiene la mayor edad: ",$row1[3],"</br>";
            $retrieveQueryMenor = "SELECT * FROM alumnos ORDER BY edad ASC limit 1;"; //MENOR ALUMNO, me asegure que solo traiga 1 en caso de que hayan 2 iguales
            $resMenor = pg_query($retrieveQueryMenor);
            $row2 = pg_fetch_row($resMenor);
            echo "MENOR alumno con nombre: ", $row2[1], ", de apellido: ",$row2[2], ", tiene la menor edad: ",$row2[3],"</br>";
        }
        returnMayorYMenor();
    ?>
</body>
</html>