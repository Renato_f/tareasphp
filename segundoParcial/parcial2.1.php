<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EJ 1 PARCIAL 2 - Renato FERRER</title>
</head>
<body>
    <?php
        /*Tema 3 – Arrays (3 puntos):
        Implementar un script PHP que haga lo siguiente:
        • Crear una función que genere códigos de 4 letras (A-Z) aleatorias.
        • Crear un array de 500 elementos cuyo índice sea numérico y cuyo valor sean
        los códigos de 4 letras generados con la función anterior.
        • Imprimir ese array utilizando la estructura foreach. Se debe imprimir de
        manera tabular indicando índice y valor del array generado aleatoriamente. */

        function generarRandomStr(){
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $result = '';
            for ($i = 0; $i < 4; $i++)
            $result .= $characters[mt_rand(0, 25)];
            return $result;
        }
        $arr = Array();
        for ($i=1; $i <= 500; $i++) { 
            $arr += [$i => generarRandomStr()];
        }
        echo "<table border='3'>";
        echo "<h2>Tabla de codigos</h2>";
        foreach ($arr as $key => $value) {
            echo "<tr style='background-color: white'>";
            echo "<td> $key </td>";
            echo "<td> $value </td>";
            echo "</tr>";
        }
        echo "</table>";
    ?>
</body>
</html>