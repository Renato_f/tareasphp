<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EJ3.7 - RF</title>
</head>
<body>
    <?php
        /*Hacer un script en PHP que determine si una cadena dada es un palíndromo
        Un palíndromo (del griego palin dromein, volver a ir hacia atrás) es una palabra, número o frase
        que se lee igual hacia adelante que hacia atrás. Si se trata de un número, se llama capicúa.
        Habitualmente, las frases palindrómicas se resienten en su significado cuanto más largas son
        Ejemplos: radar, Neuquén, anilina, ananá, Malayalam*/

        $palabra = "12321";
        /*Profe quiero dejar constancia que intente hacer manualmente el recorrido de palabra, si descomentas
        este for loop, vas a ver que me imprime la palabra al reves pero con un error, que no supe como
        solucionar!!!!!!! que rabia..
        for ($i=strlen($palabra); $i >= 0; $i--) { 
            echo $palabra[$i];
        }*/
        if(strrev($palabra) == $palabra){
            echo "Es un palindromo o un numero capicúa";
        }else {
            echo "No es ni palindromo ni un numero capicúa";
        }
        
    ?>
</body>
</html>