<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EJ3.13 - RF</title>
</head>
<body>
    <?php
        /*Crear un array asociativo de 10 elementos. Los valores de los 10 elemento del array deben ser
        strings.
        Crear un array asociativo de un elemento. El valor del array debe ser un string.
        El script PHP debe hacer lo siguiente:
        • Imprimir en pantalla si existe la clave del array de un elemento en el array de 10
        elementos (si no existe, se imprime un mensaje).
        • Imprimir en pantalla si existe el valor del vector de un elemento en el vector de 10
        elementos (si no existe, se imprime un mensaje).
        • Si no existe ni la clave ni el valor (del vector de un elemento) en el vector mayor se inserta
        como un elemento más del vector. */

        //Use como ejemplo notas de colegio
        $arr10 = array(
            "matematica" => "puntaje", "Fisica" => "Mas o menos", "Ciencias" => "Puntaje", "Religion" => "Mal",
            "Vida social" => "Puntaje", "Historia" => "Bien", "Quimica" => "Muy mal",
            "Matematica especifica" => "Muy bien", "Economia" => "Puntaje", "Programacion" => "Puntaje");
        
        $arr1 = array("Etica" => "Excelente");
        $checkBool = false;

        if(key_exists(array_key_first($arr1),$arr10)){
            echo "La clave del array con 1 elemento existe dentro de las claves del array con 10 elementos<br>";
            $checkBool = true;
        }else{
            echo "La clave del array con 1 elemento no existe dentro de las claves del array con 10 elementos<br>";
            $checkBool = false;
        }
        $aux = array_values($arr10);
        if(array_search($arr1["Etica"],$aux) == ""){
            echo "El valor del array con 1 elemento no existe dentro de los valores del array con 10 elementos<br><br>";
            
        }else{
            echo "El valor del array con 1 elemento existe dentro de los valores del array con 10 elementos<br><br>";
            $checkBool = true;
        }

        if($checkBool == false){
            $arr10 += $arr1;
        }
        foreach ($arr10 as $key => $value) {
            echo $key , ": " , $value , "<br>";
        }
    ?>  
</body> 
</html>  