<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicios#3 - Ej01</title>
</head>
<body>
        <?php
            $a = sqrt(2);
            $pi = M_PI;
            function raiz_cubica($numero){
                return pow($numero, 1/3);
            }
            $b = raiz_cubica(3);
            $c = M_EULER;
            $d = M_E;
            echo "A: $a, b: $b, pi: $pi, c: $c, d: $d";
            $x = (($a * $pi) + $b) / ($c*$d);
            echo "<br>Resultado de X es: $x";
        ?>
</body>
</html>