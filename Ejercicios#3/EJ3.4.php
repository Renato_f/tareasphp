<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EJ3.4 - RF</title>
</head>
<body>
    <?php
        /*Hacer un script PHP que haga lo siguiente:
        • Declarar 3 variable con valores enteros aleatorios (se debe generar aleatoriamente estos
        valores entre 50 y 900) – Se debe usar la función mt_srand y mt_rand.
        • Ordenar de mayor a menor los valores de estas variables.
        • Imprimir en pantalla la secuencia de números ordenadas, los números deben estar
        separados por espacios en blanco.
        • El número mayor debe estar en color verde y el número más pequeño debe estar en color
        rojo.*/
        function make_seed(){
            list($usec, $sec) = explode(' ', microtime());
            return (float) $sec + ((float) $usec * 100000);
        }
        mt_srand(make_seed());
        $var1 = mt_rand(50,900);
        $var2 = mt_rand(50,900);
        $var3 = mt_rand(50,900);
        echo "Nro random 1: $var1, Nro random 2: $var2, Nro random 3: $var3";
        $max = max($var1, $var2, $var3);
        $min = min($var1, $var2, $var3);
        echo "<br>El mayor valor es <p style='color: green'>$max</p>";
        echo "<br>El menor valor es <p style='color: red'>$min</p>"
    ?>
</body>
</html>