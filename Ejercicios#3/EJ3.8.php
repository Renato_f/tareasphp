<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EJ3.8 - RF</title>
</head>
<body>
    <?php
        /* Hacer un script en PHP que genere una matriz de dimensión n*m con números aleatorios. Las
        dimensiones n (filas) y m (columnas) son variables que debe definir el alumno. La matriz generada
        se debe imprimir en pantalla de manera tabular.*/
        function make_seed(){
            list($usec, $sec) = explode(' ', microtime());
            return (float) $sec + ((float) $usec * 100000);
        }
        mt_srand(make_seed());

        $nroColumnas = 5;
        $nroFilas = 10;

        echo "<table border='3'>";
        echo "<h2>MATRIZ</h2>";
        for ($i=0; $i < $nroFilas; $i++) { 
            echo "<tr style='background-color: white'>";
            for ($j=0; $j < $nroColumnas; $j++) { 
                $auxNum = mt_rand(1,1000);
                echo "<td> $auxNum </td>";
            }
            echo "</tr>";
        }
    ?>
</body>
</html>