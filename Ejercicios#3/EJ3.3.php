<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        /*3- Ejercicio 3:
        • Generar un script PHP que cree una tabla HTML con los números pares que existen entre 1
        y N.
        • El número N estará definido por una constante PHP. El valor de la constante N debe ser un
        número definido por el alumno.*/
        const N = 22;
        echo "<table border='3'>";
        echo "<h2>TABLA DE NUMEROS PARES </h2>";
        for ($i=1; $i <= N; $i++) { 
            if ($i%2==0) {
                echo "<tr style='background-color: white'>";
                echo "<td> $i </td>";
                echo "</tr>";
            }
        }
        echo "</table>";
    ?>
</body>
</html>