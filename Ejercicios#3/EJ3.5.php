<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EJ3.5 - RF</title>
</head>
<body>
    <?php
        /*Hacer un script PHP que genere un formulario HTML que contenga los siguientes campos: nombre,
        apellido, edad y el botón de submit.
        • Se deben usar las cadenas HEREDOC.*/
        $str = <<<IDENTIFIER
        <form>
            <label for="fname">Nombres:</label><br>
            <input type="text" id="fname" name="fname"><br>
            <label for="lname">Apellidos:</label><br>
            <input type="text" id="lname" name="lname"><br>
            <label for="ledad">Edad:</label><br>
            <input type="text" id="ledad" name="edadd"><br><br>
            <button value="Submit">Submit</button>
        </form>
        IDENTIFIER;
        echo $str
    ?>
</body>
</html>