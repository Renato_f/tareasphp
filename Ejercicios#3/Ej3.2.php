<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ej3.2 - RF</title>
</head>
<body>
    <?php
        /*2- Ejercicio 2:
        Hacer un script PHP que imprime la siguiente información:
        • Versión de PHP utilizada.
        • El id de la versión de PHP.
        • El valor máximo soportado para enteros para esa versión.
        • Tamaño máximo del nombre de un archivo.
        • Versión del Sistema Operativo.
        • Símbolo correcto de 'Fin De Línea' para la plataforma en uso.
        • El include path por defecto.
        Observación: Ver las constantes predefinidas del núcleo de PHP*/

        echo "Version de PHP: ",phpversion();
        echo "<br>Valor máximo para enteros: ",PHP_INT_MAX;
        echo "<br>Tamaño máximo del nombre del archivo: ",PHP_MAXPATHLEN;
        echo "<br>Version del SO: ",PHP_WINDOWS_VERSION_BUILD;
        echo "<br>Simbolo del fin de linea: ;";
        echo "<br>Include path por defecto: ",ini_get('include_path');
        
    ?>
</body>
</html>