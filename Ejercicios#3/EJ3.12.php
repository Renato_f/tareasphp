<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EJ3.12 - RF</title>
</head>
<body>
    <?php
        /*Hacer un script PHP que haga lo siguiente:
        • Crear un array indexado con 10 cadenas.
        • Crear una variable que tenga un string cualquiera como string.
        El script debe:
        • Buscar si la cadena declarada está en el vector declarado.
        • Si esta, se imprime “Ya existe” la cadena.
        • Si no está, se imprime “Es Nuevo” y se agrega al Final del array. */
        $arr = array("Hola", "Como vas", "Espero que bien", "Acertijos", "Renato Ferrer", "Sin sentido las cadenas?", "Cadenita", "Sol", "Agua potable","Seguridad");
        $cad = "Hola";
        $result = array_search($cad,$arr);
        if($result>-1){
            echo "Ya existe<br><br>";
        }else {
            echo "Es Nuevo<br><br>";
            array_push($arr, $cad);
        }
        for ($i=0; $i < count($arr); $i++) { 
            echo $arr[$i],"<br>";
        }
    ?>
</body>
</html>