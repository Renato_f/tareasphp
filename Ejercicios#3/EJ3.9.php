<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EJ3.9 - RF</title>
</head>
<body>
    <?php
        /*Realizar un script en PHP que declare un vector de 100 elementos con valores aleatorios enteros
        entre 1 y 100 y sume todos los valores. El script debe imprimir un mensaje con la sumatoria de los
        elementos */
        function make_seed(){
            list($usec, $sec) = explode(' ', microtime());
            return (float) $sec + ((float) $usec * 100000);
        }
        mt_srand(make_seed());

        $arr = array();
        for ($i=1; $i <= 100 ; $i++) { 
            $arr[$i] =  mt_rand(1,100);
            echo "Nro $i: <b>$arr[$i]</b><br>";
        }
        echo "La suma de todos los elementos es:", array_sum($arr);
    ?>
</body>
</html>