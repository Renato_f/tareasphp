<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EJ3.10 - RF</title>
</head>
<body>
    <?php
        /* Hacer un script PHP que declare un vector de 50 elementos con valores aleatorios enteros entre 1
        y 1000. El script debe determinar cuál es el elemento con mayor valor en el vector, se debe
        imprimir un mensaje con el valor y el índice en donde se encuentra.
        Mensaje de ejemplo: El elemento con índice 8 posee el mayor valor que 789.*/
        function make_seed(){
            list($usec, $sec) = explode(' ', microtime());
            return (float) $sec + ((float) $usec * 100000);
        }
        mt_srand(make_seed());

        $arr = array();
        $indice;
        $mayor = 0;
        for ($i=1; $i <= 50 ; $i++) { 
            $arr[$i] =  mt_rand(1,1000);
            echo "Nro $i: <b>$arr[$i]</b><br>";
            if ($arr[$i] > $mayor) {
                $mayor = $arr[$i];
                $indice = $i;
            }
        }
        echo "El elemento en la posicion: $indice, es el mayor numero de todos: $mayor";
    ?>
</body>
</html>